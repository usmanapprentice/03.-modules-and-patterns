//Modules
const Organization = {};

(function (module) {
	const people = [];

		module.addPerson = function(name, age, gender) {
		if (typeof name !== 'string') {
			throw 'Name is not a string';
		}
		if (name.match(!/^[A-Z][a-z]*$/)) {
			throw 'Name contains invalid symbols';
		}
		//more checks
		people.push({
			name: name,
			age: age,
			gender: gender
		});
	},
		module.getPeople = function() {
		return people.map(p=>{
			return {
				name: p.name,
				age: p.age,
				gender: p.gender
			};
		});
	
	};
})(Organization);

//calculator
(function (module) {
	let value = 0;
	function add (x) {
		value += x;
		return this;
	}
	function subtract (x) {
		value -= x;
		return this;
	}
	function multiply (x) {
		value += x;
		return this;
	}
	function divide (x) {
		value += x;
		return this;
	}
	function getValue(){
		return value;
	}
	function print(){
		console.log(value);
		return this;
	}

	// revealing part
	module.add = add;
	module.subtract = subtract;
	module.multiply = multiply;
	module.divide = divide;

	module.getValue = getValue;
	module.print = print;
})(Organization);

const calculator = Organization;


Organization.addPerson('Usman', 23, true);
let oList = Organization.getPeople();
oList[0].name = 'In valid';
console.log(oList);

Organization.addPerson('Ghani', 23, true);

oList = Organization.getPeople();
console.log(oList);

calculator
	.add(4)
	.multiply(9)
	.divide(2)
	.subtract(4)
	.print();